export interface PackageSchematicSchema {
  name: string
  tags?: string
  directory?: string
}

export interface BuildExecutorSchema {
  outputPath: string
  main: string
  app: string
  versionPackage: string
}
